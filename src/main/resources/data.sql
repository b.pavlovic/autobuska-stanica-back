
INSERT INTO korisnik(id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
				VALUES(1, 'b.pavlovic992@gmail.com', 'b.pavlovic', '$2y$12$P4kiIxbNDZUtudtZWMXVu.i.c8oPNAGGEEP62jYblZwz.JJY0vEoq ', 'Bojan', 'Pavlovic', 'ADMIN');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
             	 VALUES (2,'tamara@maildrop.cc','tamara','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky','Tamara','Milosavljevic','KORISNIK');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (3,'miroslav@maildrop.cc','miroslav','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6','Miroslav','Simic','ADMIN');
           
INSERT INTO prevoznik(id, naziv, adresa, pib) VALUES (1, 'Banat trans' , 'Zrenjaninski put bb', 'PIB11223344');
INSERT INTO prevoznik(id, naziv, adresa, pib) VALUES (2, 'ATP Vojvodina' , 'Kralja Petra bb', 'PIB22334455');
INSERT INTO prevoznik(id, naziv, adresa, pib) VALUES (3, 'Nis Ekspres' , 'Cara Dusana bb', 'PIB33445566');

INSERT INTO linija(id, broj_mesta, cena_karte, vreme_polaska, destinacija, prevoznik_id)
				VALUES(1, 23, 350, '07:20', 'Beograd', 1);
INSERT INTO linija(id, broj_mesta, cena_karte, vreme_polaska, destinacija, prevoznik_id)
				VALUES(2, 13, 850, '17:20', 'Nis', 3);
INSERT INTO linija(id, broj_mesta, cena_karte, vreme_polaska, destinacija, prevoznik_id)
				VALUES(3, 8, 450, '13:50', 'Novi Sad', 2);
              