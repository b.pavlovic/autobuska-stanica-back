package com.ftninformatika.jwd.test.Developing.web.dto;


public class RezervacijaDTO {
	private Long id;
	private LinijaDTO linijaDTO;
	private int brojMesta;
	
	public RezervacijaDTO() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LinijaDTO getLinijaDTO() {
		return linijaDTO;
	}

	public void setLinijaDTO(LinijaDTO linijaDTO) {
		this.linijaDTO = linijaDTO;
	}

	public int getBrojMesta() {
		return brojMesta;
	}

	public void setBrojMesta(int brojMesta) {
		this.brojMesta = brojMesta;
	}
	
	
	
}
