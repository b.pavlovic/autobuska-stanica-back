package com.ftninformatika.jwd.test.Developing.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.test.Developing.model.Linija;
import com.ftninformatika.jwd.test.Developing.model.Prevoznik;
import com.ftninformatika.jwd.test.Developing.repository.LinijaRepository;
import com.ftninformatika.jwd.test.Developing.repository.PrevoznikRepository;
import com.ftninformatika.jwd.test.Developing.service.LinijeService;

@Service
public class JpaLinijaService implements LinijeService{
	
	@Autowired
	private LinijaRepository linijaRepository;
	
	@Autowired
	private PrevoznikRepository prevoznikRepository;
	
	@Override
	public List<Linija> findAll() {
		return linijaRepository.findAll();
	}

	@Override
	public Linija findOneById(Long id) {
		return linijaRepository.findOndeById(id);
	}

	@Override
	public Page<Linija> findAll(int pageNo) {
		
		return linijaRepository.findAll(PageRequest.of(pageNo, 2));
	}

	@Override
	public Linija save(Linija linija) {
		return linijaRepository.save(linija);
	}

	@Override
	public Linija update(Linija linija) {
		return linijaRepository.save(linija);
	}

	@Override
	public void delete(Linija linija) {
		Prevoznik prevoznik = prevoznikRepository.findOneById(linija.getPrevoznik().getId());
		prevoznik.getLinije().remove(linija);
		linijaRepository.delete(linija);
	}

	@Override
	public Page<Linija> search(String destinacija, double cenaKarte, Long prevoznikId, int pageNo) {
		if(cenaKarte == 0 && prevoznikId == 0) {
			return linijaRepository.findByDestinacijaContains(destinacija, PageRequest.of(pageNo, 5));
		}else if(prevoznikId == 0) {
			return linijaRepository.findByDestinacijaContainsAndCenaKarteLessThanEqual(destinacija, cenaKarte, PageRequest.of(pageNo, 2));
		}else if(cenaKarte == 0) {
			return linijaRepository.findByDestinacijaContainsAndPrevoznikId(destinacija, prevoznikId, PageRequest.of(pageNo, 5));		}
		
		return linijaRepository.findByDestinacijaContainsAndCenaKarteLessThanAndPrevoznikId(destinacija, cenaKarte, prevoznikId, PageRequest.of(pageNo, 4));
		
	}
	
	

}
