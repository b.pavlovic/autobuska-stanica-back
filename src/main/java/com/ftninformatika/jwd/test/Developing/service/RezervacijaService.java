package com.ftninformatika.jwd.test.Developing.service;

import com.ftninformatika.jwd.test.Developing.model.Linija;
import com.ftninformatika.jwd.test.Developing.model.Rezervacija;

public interface RezervacijaService {
	Rezervacija save(Rezervacija rezervacija);
}
