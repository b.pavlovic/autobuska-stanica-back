package com.ftninformatika.jwd.test.Developing.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ftninformatika.jwd.test.Developing.model.Linija;

@Repository
public interface LinijaRepository extends JpaRepository<Linija, Long> {
	
	Linija findOndeById(Long id);
	
	Page<Linija> findAll(Pageable pageable);
	
	void delete(Linija linija);
	
	Page<Linija> findByDestinacijaContainsAndCenaKarteLessThanAndPrevoznikId(String destinacija, double cenaKarte, Long id,Pageable pageable);
	
	Page<Linija> findByDestinacijaContainsAndPrevoznikId(String destinacija, Long id,Pageable pageable);
	
	Page<Linija> findByDestinacijaContains(String destinacija,Pageable pageable);
	
	Page<Linija> findByDestinacijaContainsAndCenaKarteLessThanEqual(String destinacija, double cenaKarte,Pageable pageable);
}
