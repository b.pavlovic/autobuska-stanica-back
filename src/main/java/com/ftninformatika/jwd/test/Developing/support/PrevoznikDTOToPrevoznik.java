package com.ftninformatika.jwd.test.Developing.support;


import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.Developing.model.Prevoznik;
import com.ftninformatika.jwd.test.Developing.web.dto.PrevoznikDTO;

@Component
public class PrevoznikDTOToPrevoznik implements Converter<PrevoznikDTO, Prevoznik>{

	@Override
	public Prevoznik convert(PrevoznikDTO source) {
		Prevoznik prevoznik = new Prevoznik();
		prevoznik.setId(source.getId());
		prevoznik.setNaziv(source.getNaziv());
		prevoznik.setAdresa(source.getAdresa());
		prevoznik.setPib(source.getPib());
		return prevoznik;
	}

}
