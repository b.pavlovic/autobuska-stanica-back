package com.ftninformatika.jwd.test.Developing.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.test.Developing.model.Prevoznik;
import com.ftninformatika.jwd.test.Developing.repository.PrevoznikRepository;
import com.ftninformatika.jwd.test.Developing.service.PrevoznikService;

@Service
public class JpaPrevoznikService implements PrevoznikService{
	
	@Autowired
	private PrevoznikRepository prevoznikRepository;
	
	@Override
	public List<Prevoznik> getAll() {
		return prevoznikRepository.findAll();
	}

	@Override
	public Prevoznik findOneById(Long id) {
		return prevoznikRepository.findOneById(id);
	}

	@Override
	public Prevoznik save(Prevoznik prevoznik) {
		return prevoznikRepository.save(prevoznik);
	}

}
