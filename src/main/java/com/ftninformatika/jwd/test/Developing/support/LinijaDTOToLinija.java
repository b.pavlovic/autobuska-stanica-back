package com.ftninformatika.jwd.test.Developing.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.Developing.model.Linija;
import com.ftninformatika.jwd.test.Developing.service.PrevoznikService;
import com.ftninformatika.jwd.test.Developing.web.dto.LinijaDTO;

@Component
public class LinijaDTOToLinija implements Converter<LinijaDTO, Linija>{
	
	@Autowired
	private PrevoznikService prevoznikService;
	
	@Override
	public Linija convert(LinijaDTO source) {
		Linija linija = new Linija();
		linija.setId(source.getId());
		linija.setBrojMesta(source.getBrojMesta());
		linija.setCenaKarte(source.getCenaKarte());
		linija.setDestinacija(source.getDestinacija());
		linija.setVremePolaska(source.getVremePolaska());
		linija.setPrevoznik(prevoznikService.findOneById(source.getPrevoznikDTO().getId()));
		return linija;
	}

	

	

}
