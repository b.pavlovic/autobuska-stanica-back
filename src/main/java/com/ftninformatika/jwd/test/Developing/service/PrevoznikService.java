package com.ftninformatika.jwd.test.Developing.service;

import java.util.List;

import com.ftninformatika.jwd.test.Developing.model.Prevoznik;

public interface PrevoznikService {
	List<Prevoznik> getAll();
	Prevoznik findOneById(Long id);
	Prevoznik save(Prevoznik prevoznik);
}
