package com.ftninformatika.jwd.test.Developing.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.ftninformatika.jwd.test.Developing.model.Linija;

public interface LinijeService {
	
	List<Linija> findAll();
	
	Linija findOneById(Long id);
	
	Page<Linija> findAll(int pageNo);
	
	Linija save(Linija linija);
	
	Linija update(Linija linija);
	
	void delete(Linija linija);
	
	Page<Linija> search(String destinacija, double cenaKarte, Long prevoznikId, int pageNo);

}
