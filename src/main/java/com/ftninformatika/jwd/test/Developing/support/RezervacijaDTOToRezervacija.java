package com.ftninformatika.jwd.test.Developing.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.Developing.model.Rezervacija;
import com.ftninformatika.jwd.test.Developing.repository.LinijaRepository;
import com.ftninformatika.jwd.test.Developing.web.dto.RezervacijaDTO;

@Component
public class RezervacijaDTOToRezervacija implements Converter<RezervacijaDTO, Rezervacija> {
	
	@Autowired
	private LinijaRepository linijaRepository;
	
	@Override
	public Rezervacija convert(RezervacijaDTO source) {
		Rezervacija rez = new Rezervacija();
		rez.setBrojMesta(source.getBrojMesta());
		rez.setLinija(linijaRepository.findOndeById(source.getLinijaDTO().getId()));
		return rez;
	}
	
	
}
