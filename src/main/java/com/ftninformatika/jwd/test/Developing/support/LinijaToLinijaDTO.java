package com.ftninformatika.jwd.test.Developing.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.Developing.model.Linija;
import com.ftninformatika.jwd.test.Developing.web.dto.LinijaDTO;

@Component
public class LinijaToLinijaDTO implements Converter<Linija, LinijaDTO>{
	
	@Autowired
	private PrevoznikToPrevoznikDTO toPrevoznikDTO;
	
	@Override
	public LinijaDTO convert(Linija source) {
		LinijaDTO dto = new LinijaDTO();
		dto.setId(source.getId());
		dto.setBrojMesta(source.getBrojMesta());
		dto.setCenaKarte(source.getCenaKarte());
		dto.setDestinacija(source.getDestinacija());
		dto.setVremePolaska(source.getVremePolaska());
		dto.setPrevoznikDTO(toPrevoznikDTO.convert(source.getPrevoznik()));
		return dto;
	}
	
	public List<LinijaDTO> convert(List<Linija> linije){
		List<LinijaDTO> dtos = new ArrayList<LinijaDTO>();
		
		for (Linija linija : linije) {
			dtos.add(convert(linija));
		}
		
		return dtos;
	}

}
