package com.ftninformatika.jwd.test.Developing.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.Developing.model.Prevoznik;
import com.ftninformatika.jwd.test.Developing.web.dto.PrevoznikDTO;

@Component
public class PrevoznikToPrevoznikDTO implements Converter<Prevoznik, PrevoznikDTO>{

	@Override
	public PrevoznikDTO convert(Prevoznik source) {
		PrevoznikDTO dto = new PrevoznikDTO();
		dto.setId(source.getId());
		dto.setNaziv(source.getNaziv());
		dto.setAdresa(source.getAdresa());
		dto.setPib(source.getPib());
		return dto;
	}
	
	public List<PrevoznikDTO> convert (List<Prevoznik> prevoznici){
		List<PrevoznikDTO> dtos = new ArrayList<PrevoznikDTO>();
		
		for (Prevoznik prevoznik : prevoznici) {
			dtos.add(convert(prevoznik));
		}
		
		return dtos;
	}

}
