package com.ftninformatika.jwd.test.Developing.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.test.Developing.model.Linija;
import com.ftninformatika.jwd.test.Developing.model.Rezervacija;
import com.ftninformatika.jwd.test.Developing.service.LinijeService;
import com.ftninformatika.jwd.test.Developing.service.RezervacijaService;
import com.ftninformatika.jwd.test.Developing.support.RezervacijaDTOToRezervacija;
import com.ftninformatika.jwd.test.Developing.web.dto.RezervacijaDTO;

@RestController
@RequestMapping(value = "/api/rezervacija")
public class RezervacijaController {
	
	@Autowired
	private RezervacijaService rezervacijaService;
	
	@Autowired
	private LinijeService linijaService;

	@Autowired 
	private RezervacijaDTOToRezervacija toRezervacija;
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void>create(@RequestBody RezervacijaDTO rezervacijaDTO){
		
		Rezervacija rezervacija = toRezervacija.convert(rezervacijaDTO);
		Rezervacija sacuvanaRez = rezervacijaService.save(rezervacija);
		if(sacuvanaRez == null) {
			
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			
		}
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

}
