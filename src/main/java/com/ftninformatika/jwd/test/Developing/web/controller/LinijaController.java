package com.ftninformatika.jwd.test.Developing.web.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.test.Developing.model.Linija;
import com.ftninformatika.jwd.test.Developing.service.LinijeService;
import com.ftninformatika.jwd.test.Developing.support.LinijaDTOToLinija;
import com.ftninformatika.jwd.test.Developing.support.LinijaToLinijaDTO;
import com.ftninformatika.jwd.test.Developing.web.dto.LinijaDTO;


@RestController
@RequestMapping(value = "/api/linije")
public class LinijaController {
	
	@Autowired
	private LinijaToLinijaDTO toLinijaDTO;
	
	@Autowired
	private LinijaDTOToLinija toLinija;
	
	@Autowired
	private LinijeService linijaService;
	
	
	@GetMapping
	public ResponseEntity<List<LinijaDTO>> getAll(@RequestParam (required = false, defaultValue = "") String destinacija,
			@RequestParam(required = false, defaultValue = "0") double cenaKarte,
			@RequestParam(required =  false, defaultValue = "0") Long prevoznikId,
			@RequestParam (value = "pageNo", defaultValue = "0") int pageNo){
		
		Page<Linija> linije = linijaService.search(destinacija, cenaKarte, prevoznikId, pageNo);
		HttpHeaders headers = new HttpHeaders();
		headers.add("Total-Pages", Integer.toString(linije.getTotalPages()));
		
		return new ResponseEntity<>(toLinijaDTO.convert(linije.getContent()), headers, HttpStatus.OK);
		
	}
	
	@GetMapping(value = "{id}")
	public ResponseEntity<LinijaDTO> getOneById(@PathVariable Long id){
		Linija linija = linijaService.findOneById(id);
		if(linija == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(toLinijaDTO.convert(linija), HttpStatus.OK);
		
	}
	
	@PostMapping
	public ResponseEntity<LinijaDTO> create (@Valid @RequestBody LinijaDTO linijaDTO){
		Linija linija = toLinija.convert(linijaDTO);
		
		if (linija == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		linijaService.save(linija);
		return new ResponseEntity<>(toLinijaDTO.convert(linija), HttpStatus.CREATED);
	}
	
	@PutMapping(value = "{id}",consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LinijaDTO> update(@PathVariable Long id, @Valid @RequestBody LinijaDTO linijaDTO){
		
		if(!id.equals(linijaDTO.getId())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		Linija linija = toLinija.convert(linijaDTO);;
		linijaService.update(linija);
		
		return new ResponseEntity<>(toLinijaDTO.convert(linija), HttpStatus.OK);
		
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping(value = "{id}")
	public ResponseEntity<Void> deleteById(@PathVariable Long id){
		Linija linija = linijaService.findOneById(id);
		if(linija == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		linijaService.delete(linija);
		
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		
	}
	
//	@GetMapping(value = "/search")
//	public ResponseEntity<List<LinijaDTO>> search(@RequestParam (required = false, defaultValue = "") String destinacija,
//											@RequestParam(required = false, defaultValue = "0") double cenaMax,
//											@RequestParam(required =  false, defaultValue = "0") Long prevoznikId){
//			
//		List<Linija> linije = linijaService.search(destinacija, cenaMax, prevoznikId);
//		
//		return new ResponseEntity<>(toLinijaDTO.convert(linije), HttpStatus.OK);
//	}

}
