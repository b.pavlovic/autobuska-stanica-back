package com.ftninformatika.jwd.test.Developing.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.test.Developing.model.Linija;
import com.ftninformatika.jwd.test.Developing.model.Rezervacija;
import com.ftninformatika.jwd.test.Developing.repository.LinijaRepository;
import com.ftninformatika.jwd.test.Developing.repository.RezervacijaRepository;
import com.ftninformatika.jwd.test.Developing.service.RezervacijaService;

@Service
public class JpaRezervacijaSrervice implements RezervacijaService{
	
	@Autowired
	private RezervacijaRepository rezervacijaRepository;
	
	@Autowired
	private LinijaRepository linijaRepository;
	
	@Override
	public Rezervacija save(Rezervacija rezervacija) {
		Linija linija = rezervacija.getLinija();
		if(linija.getBrojMesta()< rezervacija.getBrojMesta()) {
			return null;
		}
		linija.setBrojMesta(linija.getBrojMesta()- rezervacija.getBrojMesta());
		
		return rezervacijaRepository.save(rezervacija);
		
	}
	
	
}
