package com.ftninformatika.jwd.test.Developing.web.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.test.Developing.model.Prevoznik;
import com.ftninformatika.jwd.test.Developing.service.PrevoznikService;
import com.ftninformatika.jwd.test.Developing.support.PrevoznikDTOToPrevoznik;
import com.ftninformatika.jwd.test.Developing.support.PrevoznikToPrevoznikDTO;
import com.ftninformatika.jwd.test.Developing.web.dto.PrevoznikDTO;

@RestController
@RequestMapping(value = "api/prevoznici")
public class PrevoznikController {
	
	@Autowired
	private PrevoznikService prevoznikService;
	
	@Autowired
	private PrevoznikToPrevoznikDTO toPrevoznikDTO;
	
	@Autowired
	private PrevoznikDTOToPrevoznik toPrevoznik;
	
	@GetMapping
	public ResponseEntity<List<PrevoznikDTO>> getAll() {
		List<Prevoznik> prevoznici = prevoznikService.getAll();
		
		return new ResponseEntity<>(toPrevoznikDTO.convert(prevoznici), HttpStatus.OK);
		
	}
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PrevoznikDTO> create(@Valid @RequestBody PrevoznikDTO prevoznikDTO){
		
		if(prevoznikDTO == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		Prevoznik prevoznik = prevoznikService.save(toPrevoznik.convert(prevoznikDTO));
		
		return new ResponseEntity<>(toPrevoznikDTO.convert(prevoznik), HttpStatus.OK);
		
	}

}
